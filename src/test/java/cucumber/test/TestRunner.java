package cucumber.test;
//import cucumber.api.CucumberOptions;
import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.api.testng.CucumberFeatureWrapper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/*import cucumber.api.CucumberOptions;
        features = "src/test/resources/features",
        glue = {"stepdefs"},
        //tags = {"~@Ignore"},
        tags={"@LoginProfile"},
        format = {
                "pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:target/cucumber-reports/json-reports/CucumberTestReport.json",
                "rerun:target/cucumber-reports/rerun-reports/rerun.txt"
        })*/
@CucumberOptions(
		features="src/test/resources/features",
		glue={"stepdefs"},
		//tags={"@LoginProfile"}
		format=
				{"pretty",
				"html:target/cucumber-reports/cucumber-pretty",
				"json:target/cucumber-reports/CucumberTestReport.json",
				"rerun:target/cucumber-reports/re-run.txt"}
		)
 public class TestRunner {
    private TestNGCucumberRunner testNGCucumberRunner;
    public static WebDriver driver;
	
    @BeforeClass(alwaysRun = true)
    public void setUpClass() throws Exception {
        
        System.setProperty("webdriver.chrome.driver", "E://chromedriver//chromedriver.exe");
   	    driver = new ChromeDriver(); 			
        driver.manage().window().maximize();			
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }

    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        testNGCucumberRunner.finish();
        driver.close();
    }
}
