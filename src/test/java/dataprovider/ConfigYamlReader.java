package dataprovider;

import java.io.File;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import pageObject.loginPageObject;

public class ConfigYamlReader {
    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
        	loginPageObject login = mapper.readValue(new File("E://a//workspace_S1-2//bdd-cucumber-testng//myseleiumproject//propertiesFiles//login.properties"), loginPageObject.class);
            System.out.println(ReflectionToStringBuilder.toString(login,ToStringStyle.MULTI_LINE_STYLE));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}


