package stepdefs;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import cucumber.test.*;
public class StepsLogin {
	
    @Given("^Open the Chrome and launch the application$")
    public void open_the_Chrome_and_launch_the_application() throws Throwable {
         TestRunner.driver.get("https://s5-5-qa.sylob.local:8443/CochiseWeb/start.do4");	
    }

    @When("^Enter the Username and Password$")
    public void enter_the_Username_and_Password() throws Throwable {
    	TestRunner.driver.findElement(By.name("login")).sendKeys("sylob");							
    	TestRunner.driver.findElement(By.name("password")).sendKeys("s#5[born2BEWild]");	
    }

    @Then("^Reset the credential$")
    public void reset_the_credential() throws Throwable {
    	TestRunner.driver.findElement(By.id("seConnecter")).click();
    }
    @Given("^The Home Page Is Opened$")
    public void the_Home_Page_Is_Opened() throws Throwable {
       TestRunner.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
       TestRunner.driver.findElement(By.id("structureBarre")).isDisplayed();
    }
    @When("^I Click On My Profil$")
    public void i_Click_On_My_Profil() throws Throwable {
        TestRunner.driver.findElement(By.id("about")).click();
        TestRunner.driver.findElement(By.xpath("(//*[@id='action_about']//li[contains(@onclick,'masquerAction')])[1]")).click();
    }
    @Then("^I Changee My Profil Configuration$")
    public void i_Changee_My_Profil_Configuration() throws Throwable {
    	TestRunner.driver.switchTo().frame("slot1");
    	TestRunner.driver.findElement(By.xpath("(*//input[@name='theme'])[3]")).click();
    	
    }
    
}
