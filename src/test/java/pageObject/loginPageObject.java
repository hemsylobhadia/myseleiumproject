package pageObject;


import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class loginPageObject {
	public loginPageObject(WebDriver driver) {
	     PageFactory.initElements(driver, this);
	 }
	
	@FindBy(name = "login")
	private WebElement login;
	@FindBy(name = "password")
	private WebElement password;
	
	public void enter_login(String mylogin) {
		login.sendKeys(mylogin);
		 }
	public void enter_password(String mypassword) {
		password.sendKeys(mypassword);
		 }


}
